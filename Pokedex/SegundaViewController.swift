//
//  SegundaViewController.swift
//  Pokedex
//
//  Created by COTEMIG on 21/03/22.
//

import UIKit

class SegundaViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaPokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let celula = tableView.dequeueReusableCell(withIdentifier: "Pokemon", for: IndexPath) as? CelulaTableViewCell{
            celula
        }
    }
    
    struct Pokemon {
        var nome: String
        var caracteristica1: String
        var caracteristica2: String
    }
    private var listaPokemon = [Pokemon]()
   
    func iniciarPokemon(){
        self.listaPokemon = [
            Pokemon(nome: "Bulbassauro", caracteristica1: "Grama", caracteristica2: "Venenoso"),
            Pokemon(nome: "Ivyssauro", caracteristica1: "Grama", caracteristica2: "Venenoso"),
            Pokemon(nome: "Venossauro", caracteristica1: "Grama", caracteristica2: "Venenoso"),
            Pokemon(nome: "Charmander", caracteristica1: "Fogo", caracteristica2:""),
            Pokemon(nome: "Charmaleon", caracteristica1: "Fogo", caracteristica2:""),
            Pokemon(nome: "Charizard", caracteristica1: "Fogo", caracteristica2:"Voador"),
            Pokemon(nome: "Squirtle", caracteristica1: "Água", caracteristica2: ""),
            Pokemon(nome: "Wartotle", caracteristica1: "Água", caracteristica2: ""),
            Pokemon(nome: "Blastoise", caracteristica1: "Água", caracteristica2: "")
            //Aluno(nome: "Alisson", matricula: "1"),
        
        
        
        ]
    }

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
       
        
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        
    }

    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        
    }

    
    @IBAction func onClick(_ sender: Any) {
        dismiss(animated: true)
    }
    
}
